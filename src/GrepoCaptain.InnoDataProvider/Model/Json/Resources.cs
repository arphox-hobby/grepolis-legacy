namespace GrepoCaptain.InnoDataProvider.Model.Json;

public record Resources
{
    public int Wood { get; init; }
    public int Stone { get; init; }
    public int Iron { get; init; }

    public int Total => Wood + Stone + Iron;
}
