using GrepoCaptain.Common.Game;
using GrepoCaptain.InnoDataProvider.Utils;

namespace GrepoCaptain.InnoDataProvider.Model;

public record Alliance : IHasBbCode
{
    public int Id { get; private init; }
    public string Name { get; private init; }
    public int Points { get; private init; }
    public int TownCount { get; private init; }
    public int MemberCount { get; private init; }
    public int Rank { get; private init; }

    public string ToBbCode()
    {
        return $"[ally]{Name}[/ally]";
    }

    internal static Alliance ParseAlliance(string row)
    {
        // Example row:
        // 40,meik+kostol+be,18946268,1381,43,1

        string[] parts = row.Split(',');

        int id = int.Parse(parts[0]);
        string name = StringUtils.DecodeString(parts[1]);
        int points = int.Parse(parts[2]);
        int townCount = int.Parse(parts[3]);
        int memberCount = int.Parse(parts[4]);
        int rank = int.Parse(parts[5]);

        return new Alliance
        {
            Id = id,
            Name = name,
            Points = points,
            TownCount = townCount,
            MemberCount = memberCount,
            Rank = rank,
        };
    }
}
