﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using GrepoCaptain.InnoDataProvider.Configuration;
using GrepoCaptain.InnoDataProvider.Utils;

namespace GrepoCaptain.InnoDataProvider.DataProvider;

public sealed class InnoDataSnapshotDownloader
{
    private readonly HttpClient _httpClient;
    private readonly InnoConfiguration _innoConfiguration;
    private readonly string _worldIdentifier;

    public InnoDataSnapshotDownloader(
        HttpClient httpClient,
        InnoConfiguration innoConfiguration,
        string worldIdentifier)
    {
        _httpClient = httpClient;
        _innoConfiguration = innoConfiguration;
        _worldIdentifier = worldIdentifier;
    }

    /// <summary>Downloads the players data text file.</summary>
    /// <returns>The lines of the data text file.</returns>
    public Task<string[]> DownloadPlayers()
        => DownloadDecompressMultiline(_innoConfiguration.Player);

    /// <summary>Downloads the towns data text file.</summary>
    /// <returns>The lines of the data text file.</returns>
    public async Task<string[]> DownloadTowns()
        => await DownloadDecompressMultiline(_innoConfiguration.Town);

    /// <summary>Downloads the islands data text file.</summary>
    /// <returns>The lines of the data text file.</returns>
    public async Task<string[]> DownloadIslands() =>
        await DownloadDecompressMultiline(_innoConfiguration.Island);

    /// <summary>Downloads the alliances data text file.</summary>
    /// <returns>The lines of the data text file.</returns>
    public async Task<string[]> DownloadAlliances() =>
        await DownloadDecompressMultiline(_innoConfiguration.Alliance);

    /// <summary>Downloads the conquers data text file.</summary>
    /// <returns>The lines of the data text file.</returns>
    public async Task<string[]> DownloadConquers() =>
        await DownloadDecompressMultiline(_innoConfiguration.Conquers);

    /// <summary>Downloads the static units json file.</summary>
    /// <returns>The downloaded json text</returns>
    public async Task<string> DownloadStaticUnits()
    {
        string url = InnoDataUrlBuilder.Build(
            _worldIdentifier,
            _innoConfiguration.Prefix,
            _innoConfiguration.StaticUnits);

        return await DownloadAndDecompress(url);
    }

    /// <summary>Downloads the static buildings json file.</summary>
    /// <returns>The downloaded json text</returns>
    public async Task<string> DownloadStaticBuildings()
    {
        string url = InnoDataUrlBuilder.Build(
            _worldIdentifier,
            _innoConfiguration.Prefix,
            _innoConfiguration.StaticBuildings);

        return await DownloadAndDecompress(url);
    }

    /// <summary>Downloads the static researches json file.</summary>
    /// <returns>The downloaded json text</returns>
    public Task<string> DownloadStaticResearches()
    {
        string url = InnoDataUrlBuilder.Build(
            _worldIdentifier,
            _innoConfiguration.Prefix,
            _innoConfiguration.StaticResearches);

        return DownloadAndDecompress(url);
    }

    private async Task<string[]> DownloadDecompressMultiline(string innoFileName)
    {
        string url = InnoDataUrlBuilder.Build(_worldIdentifier, _innoConfiguration.Prefix, innoFileName);
        string rawContent = await DownloadAndDecompress(url);
        return rawContent.Split('\n', StringSplitOptions.RemoveEmptyEntries);
    }

    private async Task<string> DownloadAndDecompress(string url)
    {
        HttpResponseMessage httpResponseMessage = await Download(url);
        Stream contentStream = await httpResponseMessage.Content.ReadAsStreamAsync();
        return GZipHelper.Decompress(contentStream);
    }

    private Task<HttpResponseMessage> Download(string url)
    {
        return _httpClient.GetAsync(url);
    }
}
