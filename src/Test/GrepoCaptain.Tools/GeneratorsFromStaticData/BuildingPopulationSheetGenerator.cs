﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GrepoCaptain.Common;
using GrepoCaptain.InnoDataProvider.Model;
using GrepoCaptain.InnoDataProvider.Model.Json.Buildings;
using MoreLinq.Extensions;
using Xunit;
using static GrepoCaptain.Tools.Utils.ClipboardHelper;

namespace GrepoCaptain.Tools.GeneratorsFromStaticData;

public sealed class BuildingPopulationSheetGenerator : ToolBase
{
    [Fact]
    public async Task Generate()
    {
        BuildingData[] buildings = (await Provider.FetchStaticBuildings())
            .OrderBy(b => b.Name)
            .ToArray();

        List<string> lines = new();

        lines.Add(string.Join('\t', new[]
        {
            "Building",
            "pop",
            "pop factor",
            "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
            "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
            "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
            "31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
            "41", "42", "43", "44", "45",
        }));

        foreach (BuildingData building in buildings)
        {
            StringBuilder sb = new();
            sb.Append(building.Name);
            sb.Append('\t');
            sb.Append(building.Population);
            sb.Append('\t');
            sb.Append(building.PopulationFactor);
            sb.Append('\t');

            for (int i = 1; i <= building.MaxLevel; i++)
            {
                double totalPopulationAtLevel = building.CalculateTotalPopulationAtLevel(i);
                totalPopulationAtLevel = Math.Round(totalPopulationAtLevel, 1);
                sb.Append(totalPopulationAtLevel.ToString(CultureInfo.CurrentCulture));
                sb.Append('\t');
            }

            lines.Add(sb.ToString());
        }


        await SetClipboard(lines);
    }
}
