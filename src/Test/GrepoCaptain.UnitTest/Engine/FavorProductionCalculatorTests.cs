﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using GrepoCaptain.Engine;
using Xunit;

namespace GrepoCaptain.UnitTest.Engine;

/// <summary>
/// Unit tests for <see cref="FavorProductionCalculator"/>.
/// </summary>
public sealed class FavorProductionCalculatorTests
{
    [Theory]
    [MemberData(nameof(DataForCalculateFavorProductionTest))]
    public void CalculateFavorProduction_test(
        int totalTempleLevels,
        bool highPriestess,
        int worldSpeed,
        double extraFavorProductionFactor,
        double expectedFavorProduction)
    {
        // Act
        double result = FavorProductionCalculator.CalculateFavorProduction(
            totalTempleLevels,
            highPriestess,
            worldSpeed,
            extraFavorProductionFactor);

        // Assert
        result.Should().BeApproximately(expectedFavorProduction, 0.1);
    }

    public static IEnumerable<object[]> DataForCalculateFavorProductionTest =>
        new List<object[]>
        {
            new object[] { 30,  false, 1, 0, 5.5 },
            new object[] { 30,  false, 1, 0.5, 8.2 },
            new object[] { 30,  false, 2, 0, 11 },
            new object[] { 30,  true, 1, 0, 8.2 },
            new object[] { 30,  true, 2, 0, 16.4 },
            new object[] { 30,  true, 2, 0.5, 21.9 },
            new object[] { 270,  true, 5, 0.5, 164.3 },     // Abdéra-verified test case
            new object[] { 259,  true, 5, 0.6, 169 },       // Abdéra-verified test case
            new object[] { 275,  true, 5, 0.5, 165.8 },     // Abdéra-verified test case
            new object[] { 1,  false, 3, 0, 3 },            // Bhrytosz-verified test case
            new object[] { 2,  false, 3, 0, 4.2 },          // Bhrytosz-verified test case
            new object[] { 3,  false, 3, 0, 5.2 },          // Bhrytosz-verified test case
            new object[] { 4,  false, 3, 0, 6 },            // Bhrytosz-verified test case
        };
}
